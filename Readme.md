# Distant Horizons

This repo is for the Distant Horizons mod.
The purpose of this submodule is to isolate code that isn't tied to a specific version of minecraft. This prevents us from having duplicate code; reducing errors and potentially helping us port to different versions faster and easier.

Check out the mod's main GitLab page here:
https://gitlab.com/jeseibel/minecraft-lod-mod

## source code installation

You shouldn't download this repo directly. 
It should be automatically included when pulling the full mod.


## Open Source Acknowledgements

XZ for Java (data compression)\
https://tukaani.org/xz/java.html

Json & Toml for Java (config handling)\
https://github.com/TheElectronWill/night-config

SVG Salamander for SVG's\
https://github.com/blackears/svgSalamander

FlatLaf for theming (Tempory to test stuff)\
https://www.formdev.com/flatlaf/
