The events api package holds objects and methods for listening to events fired by Distant Horizons'.

Each interface should only contain one method and that method should match the name of the file. This is done so Developers can mix and match what events they want their classes to handle.