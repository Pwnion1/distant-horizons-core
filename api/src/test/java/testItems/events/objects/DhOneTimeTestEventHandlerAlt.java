package testItems.events.objects;

/**
 * Dummy test event for unit tests.
 *
 * @author James Seibel
 * @version 2022-11-20
 */
public class DhOneTimeTestEventHandlerAlt extends DhOneTimeTestEventHandler
{
	// all other code should be a duplicate of the parent
}
