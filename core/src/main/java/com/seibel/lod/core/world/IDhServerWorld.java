package com.seibel.lod.core.world;

public interface IDhServerWorld
{
    void serverTick();
    void doWorldGen();
}
