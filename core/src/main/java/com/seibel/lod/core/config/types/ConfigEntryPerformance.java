package com.seibel.lod.core.config.types;

/**
 * What is the performance impact of an entry
 * (default is DONT_SHOW)
 *
 * @author coolGi
 */
public enum ConfigEntryPerformance {
    NONE,
    VERY_LOW,
    LOW,
    MEDIUM,
    HIGH,
    VERY_HIGH,

    DONT_SHOW
}
