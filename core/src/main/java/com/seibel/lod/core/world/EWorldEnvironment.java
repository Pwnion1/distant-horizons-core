package com.seibel.lod.core.world;

public enum EWorldEnvironment
{
    Client_Only,
    Client_Server,
    Server_Only
}
